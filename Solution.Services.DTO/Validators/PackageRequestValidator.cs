﻿using FluentValidation;
using Solution.Services.DTO.RequestDTO;

namespace Solution.Services.DTO.Validators
{
    class PackageRequestValidator : AbstractValidator<PackageRequest>
    {
        public PackageRequestValidator()
        {
            ///////
            RuleFor(r => r.Sender)
                .NotEmpty();

            ////////
            RuleFor(r => r.Receiver)
                .NotEmpty()
                .NotEqual(x => x.Sender);

            ///////
            RuleFor(r => r.CallId)
                .NotEmpty();

            ///////
            RuleFor(r => r.Direction)
                .IsInEnum();

            ///////
            RuleFor(r => r.Duration)
                .NotEmpty()
                .GreaterThan(0);

            ///////
            RuleFor(r => r.StartTime)
                .NotEmpty();

            ///////
            RuleFor(r => r.EndTime)
                .NotEmpty()
                .GreaterThan(x => x.StartTime);

            ///////
            RuleFor(r => r.AudioFile)
                .SetValidator(new AudioValidator());
        }
    }
}
