﻿using FluentValidation;
using Solution.Services.DTO.RequestDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Services.DTO.Validators
{
    class AudioValidator : AbstractValidator<Audio>
    {
        public AudioValidator()
        {
            RuleFor(r => r.FileName)
                .NotEmpty()
                .WithMessage("FileName can`t be empty");

            RuleFor(r => r.Md5Checksum)
                .NotEmpty()
                .WithMessage("Md5Checksum can`t be empty");
        }
    }
}
