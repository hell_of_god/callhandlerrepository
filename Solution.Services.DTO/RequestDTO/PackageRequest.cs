﻿using FluentValidation.Attributes;
using Solution.Services.DTO.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Services.DTO.RequestDTO
{
    [Validator(typeof(PackageRequestValidator))]
    public class PackageRequest
    {
        public string Sender { get; set; }

        public string Receiver { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string CallId { get; set; }

        public int Duration { get; set; }

        public Bound Direction { get; set; }

        public Audio AudioFile { get; set; }
    }
}
