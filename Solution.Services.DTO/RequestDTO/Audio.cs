﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution.Services.DTO.RequestDTO
{
    public class Audio
    {
        public string FileName { get; set; }

        public string Md5Checksum { get; set; }
    }
}
