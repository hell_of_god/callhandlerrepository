﻿using Solution.Services.DTO.RequestDTO;
using System.Threading.Tasks;

namespace Solution.Services.Interfaces.IServices
{
    public interface ICallerService
    {
        Task<bool> SaveCall(PackageRequest message, string DirectoryPath);
    }
}
