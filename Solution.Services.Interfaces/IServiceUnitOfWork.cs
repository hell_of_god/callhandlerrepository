﻿using Solution.Services.Interfaces.IServices;

namespace Solution.Services.Interfaces
{
    public interface IServiceUnitOfWork
    {
        ICallerService CallerService { get; }
    }
}
