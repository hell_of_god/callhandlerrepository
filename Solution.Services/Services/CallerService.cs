﻿using System;
using Solution.Services.Interfaces.IServices;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Solution.Services.DTO.RequestDTO;

namespace Solution.Services.Services
{
    public class CallerService : ICallerService
    {

        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pack">Data to save</param>
        /// <param name="DirectoryPath">Directory path for saving files</param>
        /// <returns></returns>
        public async Task<bool> SaveCall(PackageRequest pack, string DirectoryPath)
        {

            try
            {
                if (!Directory.Exists(DirectoryPath))
                {
                    Directory.CreateDirectory(DirectoryPath);
                }

                using (StreamWriter st = new StreamWriter(DirectoryPath + "\\" + pack.CallId + ".txt"))
                {
                    await st.WriteLineAsync("Sender=" + pack.Sender);
                    await st.WriteLineAsync("Receiver=" + pack.Receiver);
                    await st.WriteLineAsync("EndTime=" + pack.EndTime);
                    await st.WriteLineAsync("CallId=" + pack.CallId);
                    await st.WriteLineAsync("StartTime=" + pack.StartTime);
                    await st.WriteLineAsync("Duration=" + pack.Duration);
                    await st.WriteLineAsync("Direction=" + pack.Direction);
                    await st.WriteLineAsync("AudioFile=" + pack.AudioFile.FileName);
                    await st.WriteLineAsync("Md5Checksum=" + pack.AudioFile.Md5Checksum);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return true;
        }

    }

}
