﻿using Solution.Services.Interfaces;
using Solution.Services.Interfaces.IServices;

namespace Solution.Services
{
    public class ServiceUnitOfWork : IServiceUnitOfWork
    {
        private readonly ICallerService _callerService;

        public ServiceUnitOfWork(ICallerService callerService)
        {
            _callerService = callerService;
        }

        public ICallerService CallerService
        {
            get
            {
                return _callerService;
            }
        }
    }
}
