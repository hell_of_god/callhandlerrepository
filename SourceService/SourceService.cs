﻿using Newtonsoft.Json;
using NLog;
using Solution.Domain.Core;
using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace SourceService
{
    public partial class SourceService : ServiceBase
    {
        Thread workingThread;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public SourceService()
        {
            InitializeComponent();
            this.CanStop = true;
            this.CanShutdown = true;
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                DataGenerator generator = new DataGenerator("http://localhost:60576/", "api/caller/save");

                workingThread = new Thread(new ThreadStart(generator.SendData));
                workingThread.Start();


            }
            catch (System.Exception ex)
            {
                Exception tempEx = ex;

                while (tempEx.InnerException != null)
                {
                    tempEx = tempEx.InnerException;
                }

                string error = tempEx.Message;

                logger.Error(error);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
