﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Solution.Domain.Core;
using Newtonsoft.Json;
using System.Threading;
using NLog;

namespace SourceService
{
    internal class DataGenerator
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly string _serviceHostAddress;
        private readonly string _APIUrl;


        #region Fake Data
        public List<string> UserNames = new List<string>() {
            "Marilyn Monroe",
            "Abraham Lincoln",
            "Martin Luther King",
            "Bill Gates",
            "Christopher Columbus",
            "Elvis Presley",
            "Paul McCartney",
            "Pope Francis",
            "Vincent Van Gogh",
            "Rosa Parks",
            "Eva Peron",
            "Henry Ford",
            "Fidel Castro",
            "Oscar Wilde",
            "Louis Pasteur"
        };
        #endregion


        public DataGenerator(string serviceHostAddress, string APIUrl)
        {
            _serviceHostAddress = serviceHostAddress;
            _APIUrl = APIUrl;
        }

        public void SendData()
        {
            DateTime startSender = DateTime.Now;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_serviceHostAddress);

                for (int i = 0; i < 1000; i++)
                {

                    Package pack = GenerateRandomPackage();

                    try
                    {
                        HttpResponseMessage responce = client.PostAsJsonAsync(_APIUrl, pack).Result;
                        responce.EnsureSuccessStatusCode();
                    }
                    catch (Exception ex)
                    {
                        Exception tempEx = ex;
                        while (tempEx.InnerException != null)
                        {
                            tempEx = tempEx.InnerException;
                        }

                        string error = tempEx.Message;

                        logger.Error(error);
                    }

                }


            }

            DateTime endSending = DateTime.Now;
            TimeSpan diff = endSending - startSender;

            logger.Trace("Data Generator: \n 1000 requests takes " + diff.Minutes + " minutes and " + diff.Seconds + " seconds.");
        }





        private Package GenerateRandomPackage()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            string[] Names = GetRandUserNames();
            DateTime startTime = DateTime.Now;
            DateTime endTime = startTime.AddMinutes(rand.Next(2, 55)).AddSeconds(rand.Next(5, 55));

            string startTimeUTC = startTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
            string endTimeUTC = endTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");

            TimeSpan duration = endTime - startTime;

            string CallId = Guid.NewGuid().ToString();

            var randval = rand.Next(0, 10) > 5 ? "InBound" : "OutBound";


            string direction = randval;

            string md5Checksum;
            
            using (var md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(CallId));
                var hashstr = BitConverter.ToString(hash);
                var replased = hashstr.Replace("-", string.Empty).ToLowerInvariant();
                md5Checksum = replased;
            }

            Audio audioFile = new Audio(CallId + ".wav", md5Checksum);

            Package pack = new Package(Names[0], Names[1], startTimeUTC, endTimeUTC, CallId, (int)duration.TotalSeconds, direction, audioFile);

            return pack;
        }



        private string[] GetRandUserNames()
        {
            Random rand = new Random();
            string randSender = UserNames[rand.Next(0, 14)];
            string randReceiver;
            do
            {
                randReceiver = UserNames[rand.Next(0, 14)];

            } while (randSender == randReceiver);

            string[] NamesPair = new[] { randSender, randReceiver };

            return NamesPair;
        }


    }
}