﻿namespace SourceService
{
    internal class Audio
    {
        public Audio(string fileName, string md5Checksum)
        {

            FileName = fileName;
            Md5Checksum = md5Checksum;
        }

        public string FileName { get; private set; }

        public string Md5Checksum { get; private set; }
    }
}