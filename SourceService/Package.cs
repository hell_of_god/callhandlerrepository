﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SourceService
{
    internal class Package
    {
        public Package(string sender, string receiver, string startTime, string endTime, string callId, int duration, string direction, Audio audioFile)
        {
            Sender = sender;
            Receiver = receiver;
            StartTime = startTime;
            EndTime = endTime;
            CallId = callId;
            Duration = duration;
            AudioFile = audioFile;
            Direction = direction;
        }


        public string Sender { get; private set; }

        public string Receiver { get; private set; }

        public string StartTime { get; private set; }

        public string EndTime { get; private set; }

        public string CallId { get; private set; }

        public int Duration { get; private set; }

        public string Direction { get; private set; }

        public Audio AudioFile { get; private set; }

    }
}