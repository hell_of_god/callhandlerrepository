﻿using NLog;
using NLog.Internal;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RequestHandler
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        protected void Application_Start()
        {
            var webConfig = System.Configuration.ConfigurationManager.AppSettings["FilesHoldingDirectory"].ToString();
            logger.Trace("webConfig: " + webConfig);

            AreaRegistration.RegisterAllAreas();

            //Start IoC 
            UnityWebApiActivator.Start();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
