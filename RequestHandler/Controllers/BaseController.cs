﻿using Solution.Services.Interfaces;
using System.Web.Http;

namespace RequestHandler.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected readonly IServiceUnitOfWork _serviceUoW;

        public BaseController(IServiceUnitOfWork serviceUoW)
        {
            _serviceUoW = serviceUoW;
        }
    }
}
