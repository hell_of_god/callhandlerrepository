﻿using Newtonsoft.Json;
using NLog;
using RequestHandler.Filters;
using Solution.Services.DTO.RequestDTO;
using Solution.Services.Interfaces;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;


namespace RequestHandler.Controllers
{
    [RoutePrefix("api/caller")]
    public class CallerController : BaseController
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public CallerController(IServiceUnitOfWork serviceUoW) : base(serviceUoW)
        {
        }

        [HttpPost]
        [Route("save")]
        [ValidateModelStateFilter]
        public async Task<IHttpActionResult> SaveData(PackageRequest pack)
        {
            bool result;

            try
            {
                string PathUrl = ConfigurationManager.AppSettings["FilesHoldingDirectory"].ToString();

                result = await _serviceUoW.CallerService.SaveCall(pack, PathUrl);

            }
            catch (System.Exception ex)
            {
                logger.Error(ex.Message);
                return BadRequest();

            }

            return Ok(result);
        }
    }
}
